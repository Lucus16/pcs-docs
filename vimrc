set mouse=a                 " mouse support
syntax on                   " syntax highlighting
set ai                      " copy indentation from previous line
filetype plugin indent on   " indentation based on filetype
filetype plugin on          " custom functions based on filetype
set bg=dark                 " dark color scheme
set title                   " title shows file being edited
set ts=4 sw=4 sts=4         " set tab width to 4
set pt=<Insert>             " toggle paste mode with insert
set bk bdir=~/tmp/vim       " keep a backup file in ~/tmp/vim on every write
set ru sc                   " always show cursor pos and incomplete command
tab all                     " open multiple files in tabs
set nojs                    " no double spaces in formatting
set tw=80                   " limit text width to 80 columns

" highlight matching parentheses and fix diff colors
hi MatchParen cterm=bold ctermfg=cyan ctermbg=none
hi diffAdded ctermfg=green
hi diffRemoved ctermfg=red

" use tab and shift tab to change indentation anywhere in line
nnoremap <Tab> >>
nnoremap <S-Tab> <<
inoremap <S-Tab> <C-D>
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv
" copy and paste, insert literal now on ctrl l
inoremap <C-L> <C-V>
vnoremap <C-C> "+y
vnoremap <C-X> "+d
vnoremap <C-V> d"+p
inoremap <C-V> "+p
nnoremap <C-V> "+p
" switch tabs
nnoremap <A-Left> :tabp<CR>
nnoremap <A-Right> :tabn<CR>
inoremap <A-Left> :tabp<CR>
inoremap <A-Right> :tabn<CR>
vnoremap <A-Left> :tabp<CR>
vnoremap <A-Right> :tabn<CR>
