# Set target with $ export X=a or override with $ make b

all: .$(X).diff
	[ ! -s .$(X).diff ] || vim .$(X).diff

.$(X).bin: $(X).cpp
	g++ -g -O2 -std=gnu++0x -static $(X).cpp -o .$(X).bin

.$(X).result: .$(X).bin $(X).in
	./.$(X).bin < $(X).in > .$(X).result

.$(X).diff: .$(X).result $(X).out
	diff -u .$(X).result $(X).out > .$(X).diff; true

%:
	X=$* make
